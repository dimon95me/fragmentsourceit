package com.example.student.frames;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;

public class MainActivity extends AppCompatActivity implements Showable {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, InputFragmentt.newInstance("Hello from activity"))
                .commit();
    }

    @Override
    public void show() {
        Toast.makeText(this, "show", Toast.LENGTH_SHORT);
    }
}
