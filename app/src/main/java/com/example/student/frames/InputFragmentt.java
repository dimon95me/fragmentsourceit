package com.example.student.frames;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;


public class InputFragmentt extends Fragment {

    public static final String EXTRA_TEXT = "extra text";

    public static InputFragmentt newInstance(String text){
        InputFragmentt fragmentt = new InputFragmentt();
        Bundle args = new Bundle();
        args.putString(EXTRA_TEXT, text);
        fragmentt.setArguments(args);
        return fragmentt;
    }


    String welcome;
    Showable showable;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof MainActivity){
            showable = (Showable) context;
        }
        else {
            throw new IllegalArgumentException(
                    context.getClass().getSimpleName() +
                            "must impleemnt Showable");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_input_fragmentt, container, false);
        ButterKnife.bind(this, root);

        text = (TextView) root.findViewById(R.id.text);
        return root;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        text.setText(welcome);
    }

    @BindView(R.id.text)
    TextView text;




}
