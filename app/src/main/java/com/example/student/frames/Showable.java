package com.example.student.frames;

public interface Showable {
    void show();
}
